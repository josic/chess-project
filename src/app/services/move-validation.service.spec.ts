import { TestBed } from '@angular/core/testing';

import { MoveValidationService } from './move-validation.service';

describe('MoveValidationService', () => {
  let service: MoveValidationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MoveValidationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
