import { Injectable } from '@angular/core';
import { Properties } from '../classes/properties';
import { Coordinates } from '../classes/coordinates';
import { Color } from '../constants/enums.enum';
import { Piece } from '../classes/piece';
import { King } from '../classes/king';

@Injectable({
  providedIn: 'root'
})
export class MoveValidationService {

  board:Properties[][];

  constructor() { }


  isMovableSquare(move:Coordinates,piece:Piece){
    return this.isUnoccupiedSquare(move) || this.squareContainsEnemyPiece(move,piece.color);
  }
  isUnoccupiedSquare(move:Coordinates):Boolean{
    return this.board[move.number-1][move.letter.charCodeAt(0)-97].piece == null;
  }

  squareContainsEnemyPiece(move:Coordinates,color:Color):Boolean{
    try{
      return this.board[move.number-1][move.letter.charCodeAt(0)-97].piece.color != color;
    }
    catch{
      return false;
    }
  }

  squareContainsAllyPiece(move:Coordinates,color:Color):Boolean{
    try{
      return this.board[move.number-1][move.letter.charCodeAt(0)-97].piece.color == color;
    }
    catch{
      return false;
    }
  }

  findValidMoves(piece:Piece){
    piece.validMoves = [];
    piece.findPossibleMoves();
    switch (piece.constructor.name) {
      case 'Pawn': {
          this.findPawnValidMoves(piece);
          break;
      }
      case 'Knight': {
          this.findKnightValidMoves(piece);
          break;
      }
      case 'Queen': {
          this.findQueenValidMoves(piece);
          break;
      }
      case 'Bishop': {
        this.findBishopValidMoves(piece);
        break;
      }
      case 'Rook': {
        this.findRookValidMoves(piece);
        break;
      }
      case 'King': {
        this.findKingValidMoves(piece);
        break;
      }
      default: {
          console.log('unknownPiece');
      }
    }
  }

  isValidMove(piece:Piece,row:number,square:number) : Boolean{
    this.findValidMoves(piece);
    for(let move of piece.validMoves){
      if(move.letter == this.board[row][square].letter && move.number == this.board[row][square].squareNumber && this.isNotInCheckAfterMove(move,piece)){
        console.log("isvalidMove")
        return true;
      }
    }
    console.log("isnotvalidMove")
    return false;
  }

  findPawnValidMoves(piece:Piece){
    let topMoves = []
    for(let move of piece.possibleMoves){
      if(move.letter != piece.position.letter){
        if(this.squareContainsEnemyPiece(move,piece.color)){
          piece.validMoves.push(move);
        }
      }
      else{
        topMoves.push(move);
      }
    }
    for(let move of topMoves){
      if(this.isUnoccupiedSquare(move)){
        piece.validMoves.push(move);
      }
      else{
        break;
      }
    }
  }

  findKnightValidMoves(piece:Piece){
    for(let move of piece.possibleMoves){
      if(this.isMovableSquare(move,piece)){
        piece.validMoves.push(move);
      }
    }
  }

  findBishopValidMoves(piece:Piece){
    let topRightDiagonal = [];
    let topLeftDiagonal = [];
    let bottomRightDiagonal = [];
    let bottomLeftDiagonal = [];
    for(let move of piece.possibleMoves){
      if(move.letter.charCodeAt(0) > piece.position.letter.charCodeAt(0) && move.number > piece.position.number){
        topRightDiagonal.push(move);
      }
      else if(move.letter.charCodeAt(0) < piece.position.letter.charCodeAt(0) && move.number > piece.position.number){
        topLeftDiagonal.push(move);
      }
      else if(move.letter.charCodeAt(0) > piece.position.letter.charCodeAt(0) && move.number < piece.position.number){
        bottomRightDiagonal.push(move);
      }
      else if(move.letter.charCodeAt(0) < piece.position.letter.charCodeAt(0) && move.number < piece.position.number){
        bottomLeftDiagonal.push(move);
      }
    }
    this.findLinearMoves(topRightDiagonal,piece);
    this.findLinearMoves(topLeftDiagonal,piece);
    this.findLinearMoves(bottomRightDiagonal,piece);
    this.findLinearMoves(bottomLeftDiagonal,piece);
  }

  findLinearMoves(moves:Coordinates[],piece:Piece){
    for(let move of moves){
      if(this.isMovableSquare(move,piece)){
        piece.validMoves.push(move);
      }
      else{
        break;
      }
    }
  }
  findRookValidMoves(piece:Piece){
    let topMoves = [];
    let bottomMoves = [];
    let leftMoves = [];
    let rightMoves = [];
    for(let move of piece.possibleMoves){
      if(move.letter.charCodeAt(0) == piece.position.letter.charCodeAt(0) && move.number > piece.position.number){
        topMoves.push(move);
      }
      else if(move.letter.charCodeAt(0) == piece.position.letter.charCodeAt(0) && move.number < piece.position.number){
        bottomMoves.push(move);
      }
      else if(move.letter.charCodeAt(0) < piece.position.letter.charCodeAt(0) && move.number == piece.position.number){
        leftMoves.push(move);
      }
      else if(move.letter.charCodeAt(0) > piece.position.letter.charCodeAt(0) && move.number == piece.position.number){
        rightMoves.push(move);
      }
    }
    this.findLinearMoves(topMoves,piece);
    this.findLinearMoves(bottomMoves,piece);
    this.findLinearMoves(leftMoves,piece);
    this.findLinearMoves(rightMoves,piece);
  }

  findQueenValidMoves(piece:Piece){
    this.findBishopValidMoves(piece);
    this.findRookValidMoves(piece);
  }

  canCastle(piece:Piece,row:number){
    console.log(this.board[row][7])
    let hEdgePiece = this.board[row][7].piece;
    if(hEdgePiece.constructor.name == "Rook" && hEdgePiece.color == piece.color && !hEdgePiece.hasMoved){
      if(this.isUnoccupiedSquare({letter:'f',number:row+1}) && this.isUnoccupiedSquare({letter:'g',number:row+1}) && this.isNotInCheckAfterMove({letter:'f',number:row+1},piece) && this.isNotInCheckAfterMove({letter:'g',number:row+1},piece)){
        piece.validMoves.push({letter:'g',number:row+1});
      }
    }
    let aEdgePiece = this.board[row][0].piece;
    if(aEdgePiece.constructor.name == "Rook" && aEdgePiece.color == piece.color && !aEdgePiece.hasMoved){
      if(this.isUnoccupiedSquare({letter:'d',number:row+1}) && this.isUnoccupiedSquare({letter:'c',number:row+1}) && this.isUnoccupiedSquare({letter:'b',number:row+1}) && this.isNotInCheckAfterMove({letter:'d',number:row+1},piece) && this.isNotInCheckAfterMove({letter:'c',number:row+1},piece)){
        piece.validMoves.push({letter:'c',number:row+1});
      }
    }
  }
  findKingValidMoves(piece:Piece){
    if(!piece.hasMoved){
      if(piece.color == Color.Black){
        this.canCastle(piece,7);
      }
      else{
        this.canCastle(piece,0);
      }
    }
    console.log(piece.validMoves);
    this.findKnightValidMoves(piece);
  }

  isNotInCheckAfterMove(move:Coordinates, piece:Piece){
    let initialPiecePosition = {letter:piece.position.letter,number:piece.position.number};
    let oldPiece = this.board[move.number-1][move.letter.charCodeAt(0)-97].piece;
    this.board[move.number-1][move.letter.charCodeAt(0)-97].piece = piece;
    this.board[piece.position.number-1][piece.position.letter.charCodeAt(0)-97].piece = null;
    piece.position = {number:this.board[move.number-1][move.letter.charCodeAt(0)-97].squareNumber,letter:this.board[move.number-1][move.letter.charCodeAt(0)-97].letter};
    if(!this.isInCheck(piece.color)){
      this.board[initialPiecePosition.number-1][initialPiecePosition.letter.charCodeAt(0)-97].piece = piece;
      this.board[move.number-1][move.letter.charCodeAt(0)-97].piece = oldPiece;
      piece.position = initialPiecePosition;
      return true;
    }
    else{
      this.board[initialPiecePosition.number-1][initialPiecePosition.letter.charCodeAt(0)-97].piece = piece;
      this.board[move.number-1][move.letter.charCodeAt(0)-97].piece = oldPiece;
      piece.position = initialPiecePosition;
      return false;
    }
  }

  lookForKing(color:Color){
    for(let row of this.board){
      for(let square of row){
        if(square.piece != null && square.piece.constructor.name == 'King' && square.piece.color == color){
          return square.piece.position;
        }
      }
    }
  }

  lookForCheckmate(color:Color){
    for(let row of this.board){
      for(let square of row){
        if(this.squareContainsAllyPiece({letter:square.letter,number:square.squareNumber},color)){
          this.findValidMoves(square.piece);
          for(let move of square.piece.validMoves){
            if(this.isNotInCheckAfterMove(move,square.piece)){
              console.log('check but no checkmate')
              return false;
            }
          }
        }
      }
    }
    console.log('checkmate')
    return true;
  }

  lookForStalemate(color:Color){
    for(let row of this.board){
      for(let square of row){
        if(this.squareContainsAllyPiece({letter:square.letter,number:square.squareNumber},color)){
          this.findValidMoves(square.piece);
          let realValidMoves = [];
          for(let move of square.piece.validMoves){
            if(this.isNotInCheckAfterMove(move,square.piece)){
              realValidMoves.push(move);
            }
          }
          if(realValidMoves.length != 0){
            console.log('no stalemate')
            return false;
          }
        }
      }
    }
    console.log('stalemate');
    return true;
  }

  isInCheck(color:Color){
    let kingPosition = this.lookForKing(color);
    if(!this.diagonalChecksExist(kingPosition,color) && !this.straightChecksExist(kingPosition,color) && !this.knightChecksExist(kingPosition,color) && !this.pawnChecksExist(kingPosition,color) && !this.kingChecksExist(kingPosition,color)){
      return false;
    }
    console.log('check found')
    return true;
  }

  diagonalChecksExist(kingPosition:Coordinates, color:Color){
    let newPiece = new King(Color.Black,{letter:'d',number:8});
    let newPosition = {letter:kingPosition.letter,number:kingPosition.number + 1};
    let letterCode = kingPosition.letter.charCodeAt(0) + 1;
    while(newPiece.isPossibleMove({letter:String.fromCharCode(letterCode),number:newPosition.number})){
      if(this.squareContainsEnemyPiece({letter:String.fromCharCode(letterCode),number:newPosition.number},color)){
        if(this.board[newPosition.number-1][letterCode-97].piece.constructor.name == 'Queen' || this.board[newPosition.number-1][letterCode-97].piece.constructor.name == 'Bishop'){
          console.log('bishop or queen check')
          console.log("is checking from this position:")
          console.log(this.board[newPosition.number-1][letterCode-97].piece.position)
          return true;
        }
        else{
          break;
        }
      }
      else if(this.squareContainsAllyPiece({letter:String.fromCharCode(letterCode),number:newPosition.number},color)){
        break;
      }
      letterCode++;
      newPosition.number++;
    }

    newPosition.number = kingPosition.number;
    newPosition.number -= 1;
    letterCode = kingPosition.letter.charCodeAt(0) - 1;
    while(newPiece.isPossibleMove({letter:String.fromCharCode(letterCode),number:newPosition.number})){
      if(this.squareContainsEnemyPiece({letter:String.fromCharCode(letterCode),number:newPosition.number},color)){
        if(this.board[newPosition.number-1][letterCode-97].piece.constructor.name == 'Queen' || this.board[newPosition.number-1][letterCode-97].piece.constructor.name == 'Bishop'){
          console.log('bishop or queen check')
          console.log("is checking from this position:")
          console.log(this.board[newPosition.number-1][letterCode-97].piece.position)
          return true;
        }
        else{
          break;
        }
      }
      else if(this.squareContainsAllyPiece({letter:String.fromCharCode(letterCode),number:newPosition.number},color)){
        break;
      }
      letterCode--;
      newPosition.number--;
    }

    newPosition.number = kingPosition.number;
    newPosition.number -= 1;
    letterCode = kingPosition.letter.charCodeAt(0) + 1;
    while(newPiece.isPossibleMove({letter:String.fromCharCode(letterCode),number:newPosition.number})){
      if(this.squareContainsEnemyPiece({letter:String.fromCharCode(letterCode),number:newPosition.number},color)){
        if(this.board[newPosition.number-1][letterCode-97].piece.constructor.name == 'Queen' || this.board[newPosition.number-1][letterCode-97].piece.constructor.name == 'Bishop'){
          console.log('bishop or queen check')
          console.log("is checking from this position:")
          console.log(this.board[newPosition.number-1][letterCode-97].piece.position)
          return true;
        }
        else{
          break;
        }
      }
      else if(this.squareContainsAllyPiece({letter:String.fromCharCode(letterCode),number:newPosition.number},color)){
        break;
      }
      letterCode++;
      newPosition.number--;
    }

    newPosition.number = kingPosition.number;
    newPosition.number += 1;
    letterCode = kingPosition.letter.charCodeAt(0) - 1;
    while(newPiece.isPossibleMove({letter:String.fromCharCode(letterCode),number:newPosition.number})){
      if(this.squareContainsEnemyPiece({letter:String.fromCharCode(letterCode),number:newPosition.number},color)){
        if(this.board[newPosition.number-1][letterCode-97].piece.constructor.name == 'Queen' || this.board[newPosition.number-1][letterCode-97].piece.constructor.name == 'Bishop'){
          console.log('bishop or queen check')
          console.log("is checking from this position:")
          console.log(this.board[newPosition.number-1][letterCode-97].piece.position)
          return true;
        }
        else{
          break;
        }
      }
      else if(this.squareContainsAllyPiece({letter:String.fromCharCode(letterCode),number:newPosition.number},color)){
        break;
      }
      letterCode--;
      newPosition.number++;
    }
    return false;
}

  straightChecksExist(kingPosition:Coordinates, color:Color){
    let newPiece = new King(Color.Black,{letter:'d',number:8});
    let newPosition = {letter:kingPosition.letter,number:kingPosition.number};
    newPosition.number += 1;
    //vertical
    while(newPiece.isPossibleMove(newPosition)){
      if(this.squareContainsEnemyPiece(newPosition,color)){
        if(this.board[newPosition.number-1][newPosition.letter.charCodeAt(0)-97].piece.constructor.name == 'Queen' || this.board[newPosition.number-1][newPosition.letter.charCodeAt(0)-97].piece.constructor.name == 'Rook'){
          console.log('rook or queen check')
          return true;
        }
        else{
          break;
        }
      }
      else if(this.squareContainsAllyPiece(newPosition,color)){
        break;
      }
      newPosition.number++;
    }

    newPosition = {letter:kingPosition.letter,number:kingPosition.number};
    newPosition.number -= 1;
    while(newPiece.isPossibleMove(newPosition)){
      if(this.squareContainsEnemyPiece(newPosition,color)){
        if(this.board[newPosition.number-1][newPosition.letter.charCodeAt(0)-97].piece.constructor.name == 'Queen' || this.board[newPosition.number-1][newPosition.letter.charCodeAt(0)-97].piece.constructor.name == 'Rook'){
          console.log('rook or queen check')
          return true;
        }
        else{
          break;
        }
      }
      else if(this.squareContainsAllyPiece(newPosition,color)){
        break;
      }
      newPosition.number--;
    }
    //horizontal
    let letterCode = kingPosition.letter.charCodeAt(0) - 1;
    newPosition.number = kingPosition.number
    while(letterCode > 96){
      if(this.squareContainsEnemyPiece({letter:String.fromCharCode(letterCode),number:newPosition.number},color)){
        if(this.board[newPosition.number-1][letterCode-97].piece.constructor.name == 'Queen' || this.board[newPosition.number-1][letterCode-97].piece.constructor.name == 'Rook'){
          console.log('rook or queen check')
          return true;
        }
        else{
          break;
        }
      }
      else if(this.squareContainsAllyPiece({letter:String.fromCharCode(letterCode),number:newPosition.number},color)){
        break;
      }
      letterCode --;
    }

    letterCode = kingPosition.letter.charCodeAt(0) + 1;
    while(String.fromCharCode(letterCode) != 'i'){
      if(this.squareContainsEnemyPiece({letter:String.fromCharCode(letterCode),number:newPosition.number},color)){
        if(this.board[newPosition.number-1][letterCode-97].piece.constructor.name == 'Queen' || this.board[newPosition.number-1][letterCode-97].piece.constructor.name == 'Rook'){
          console.log('rook or queen check')
          return true;
        }
        else{
          break;
        }
      }

      else if(this.squareContainsAllyPiece({letter:String.fromCharCode(letterCode),number:newPosition.number},color)){
        break;
      }
      letterCode++;
    }
    return false;
  }

  knightChecksExist(kingPosition:Coordinates,color:Color):Boolean{
    let newPiece = new King(Color.Black,{letter:'d',number:8});
    let newNumber = kingPosition.number + 1;
    let letterCode = kingPosition.letter.charCodeAt(0) - 2;
    if(newPiece.isPossibleMove({letter:String.fromCharCode(letterCode),number:newNumber})){
      if(this.squareContainsEnemyPiece({letter:String.fromCharCode(letterCode),number:newNumber},color)){
        if(this.board[newNumber-1][letterCode-97].piece.constructor.name == 'Knight'){
          console.log('knight check')
          return true;
        }
      }
    }

    letterCode = kingPosition.letter.charCodeAt(0) + 2;
    if(newPiece.isPossibleMove({letter:String.fromCharCode(letterCode),number:newNumber})){
      if(this.squareContainsEnemyPiece({letter:String.fromCharCode(letterCode),number:newNumber},color)){
        if(this.board[newNumber-1][letterCode-97].piece.constructor.name == 'Knight'){
          console.log('knight check')
          return true;
        }
      }
    }

    newNumber = kingPosition.number + 2;
    letterCode = kingPosition.letter.charCodeAt(0) - 1;
    if(newPiece.isPossibleMove({letter:String.fromCharCode(letterCode),number:newNumber})){
      if(this.squareContainsEnemyPiece({letter:String.fromCharCode(letterCode),number:newNumber},color)){
        if(this.board[newNumber-1][letterCode-97].piece.constructor.name == 'Knight'){
          console.log('knight check')
          return true;
        }
      }
    }

    letterCode = kingPosition.letter.charCodeAt(0) + 1;
    if(newPiece.isPossibleMove({letter:String.fromCharCode(letterCode),number:newNumber})){
      if(this.squareContainsEnemyPiece({letter:String.fromCharCode(letterCode),number:newNumber},color)){
        if(this.board[newNumber-1][letterCode-97].piece.constructor.name == 'Knight'){
          console.log('knight check')
          return true;
        }
      }
    }

    newNumber = kingPosition.number - 2;
    letterCode = kingPosition.letter.charCodeAt(0) - 1;
    if(newPiece.isPossibleMove({letter:String.fromCharCode(letterCode),number:newNumber})){
      if(this.squareContainsEnemyPiece({letter:String.fromCharCode(letterCode),number:newNumber},color)){
        if(this.board[newNumber-1][letterCode-97].piece.constructor.name == 'Knight'){
          console.log('knight check')
          return true;
        }
      }
    }

    letterCode = kingPosition.letter.charCodeAt(0) + 1;
    if(newPiece.isPossibleMove({letter:String.fromCharCode(letterCode),number:newNumber})){
      if(this.squareContainsEnemyPiece({letter:String.fromCharCode(letterCode),number:newNumber},color)){
        if(this.board[newNumber-1][letterCode-97].piece.constructor.name == 'Knight'){
          console.log('knight check')
          return true;
        }
      }
    }

    newNumber = kingPosition.number - 1;
    letterCode = kingPosition.letter.charCodeAt(0) - 2;
    if(newPiece.isPossibleMove({letter:String.fromCharCode(letterCode),number:newNumber})){
      if(this.squareContainsEnemyPiece({letter:String.fromCharCode(letterCode),number:newNumber},color)){
        if(this.board[newNumber-1][letterCode-97].piece.constructor.name == 'Knight'){
          console.log('knight check')
          return true;
        }
      }
    }

    letterCode = kingPosition.letter.charCodeAt(0) + 2;
    if(newPiece.isPossibleMove({letter:String.fromCharCode(letterCode),number:newNumber})){
      if(this.squareContainsEnemyPiece({letter:String.fromCharCode(letterCode),number:newNumber},color)){
        if(this.board[newNumber-1][letterCode-97].piece.constructor.name == 'Knight'){
          console.log('knight check')
          return true;
        }
      }
    }
    return false;
  }

  pawnChecksExist(kingPosition:Coordinates,color:Color):Boolean{
    let newPiece = new King(Color.Black,{letter:'d',number:8});
    if(color == Color.Black){
      let pawnPosition = {letter:String.fromCharCode(kingPosition.letter.charCodeAt(0)-1),number:kingPosition.number-1};
      if(newPiece.isPossibleMove({letter:pawnPosition.letter,number:pawnPosition.number})){
        if(this.squareContainsEnemyPiece({letter:pawnPosition.letter,number:pawnPosition.number},color)){
          if(this.board[pawnPosition.number-1][pawnPosition.letter.charCodeAt(0)-97].piece.constructor.name == 'Pawn'){
            return true
          }
        }
      }
      pawnPosition.letter = String.fromCharCode(pawnPosition.letter.charCodeAt(0)+2);
      if(newPiece.isPossibleMove({letter:pawnPosition.letter,number:pawnPosition.number})){
        if(this.squareContainsEnemyPiece({letter:pawnPosition.letter,number:pawnPosition.number},color)){
          if(this.board[pawnPosition.number-1][pawnPosition.letter.charCodeAt(0)-97].piece.constructor.name == 'Pawn'){
            return true
          }
        }
      }
    }
    else{
      let pawnPosition = {letter:String.fromCharCode(kingPosition.letter.charCodeAt(0)-1),number:kingPosition.number+1};
      if(newPiece.isPossibleMove({letter:pawnPosition.letter,number:pawnPosition.number})){
        if(this.squareContainsEnemyPiece({letter:pawnPosition.letter,number:pawnPosition.number},color)){
          if(this.board[pawnPosition.number-1][pawnPosition.letter.charCodeAt(0)-97].piece.constructor.name == 'Pawn'){
            return true
          }
        }
      }
      pawnPosition.letter = String.fromCharCode(pawnPosition.letter.charCodeAt(0)+2);
      if(newPiece.isPossibleMove({letter:pawnPosition.letter,number:pawnPosition.number})){
        if(this.squareContainsEnemyPiece({letter:pawnPosition.letter,number:pawnPosition.number},color)){
          if(this.board[pawnPosition.number-1][pawnPosition.letter.charCodeAt(0)-97].piece.constructor.name == 'Pawn'){
            return true
          }
        }
      }
    }
  }

  kingChecksExist(kingPosition:Coordinates,color:Color):Boolean{
    let newPiece = new King(Color.Black,{letter:'d',number:8});
    let newNumber = kingPosition.number + 1;
    let letterCode = kingPosition.letter.charCodeAt(0) - 1;
    if(newPiece.isPossibleMove({letter:String.fromCharCode(letterCode),number:newNumber})){
      if(this.squareContainsEnemyPiece({letter:String.fromCharCode(letterCode),number:newNumber},color)){
        if(this.board[newNumber-1][letterCode-97].piece.constructor.name == 'King'){
          console.log('King check')
          return true;
        }
      }
    }

    newNumber = kingPosition.number + 1;
    if(newPiece.isPossibleMove({letter:String.fromCharCode(letterCode),number:newNumber})){
      if(this.squareContainsEnemyPiece({letter:String.fromCharCode(letterCode),number:newNumber},color)){
        if(this.board[newNumber-1][letterCode-97].piece.constructor.name == 'King'){
          console.log('King check')
          return true;
        }
      }
    }

    newNumber = kingPosition.number + 1;
    letterCode = kingPosition.letter.charCodeAt(0) + 1;
    if(newPiece.isPossibleMove({letter:String.fromCharCode(letterCode),number:newNumber})){
      if(this.squareContainsEnemyPiece({letter:String.fromCharCode(letterCode),number:newNumber},color)){
        if(this.board[newNumber-1][letterCode-97].piece.constructor.name == 'King'){
          console.log('King check')
          return true;
        }
      }
    }

    letterCode = kingPosition.letter.charCodeAt(0) + 1;
    if(newPiece.isPossibleMove({letter:String.fromCharCode(letterCode),number:newNumber})){
      if(this.squareContainsEnemyPiece({letter:String.fromCharCode(letterCode),number:newNumber},color)){
        if(this.board[newNumber-1][letterCode-97].piece.constructor.name == 'King'){
          console.log('King check')
          return true;
        }
      }
    }

    newNumber = kingPosition.number - 1;
    letterCode = kingPosition.letter.charCodeAt(0) + 1;
    if(newPiece.isPossibleMove({letter:String.fromCharCode(letterCode),number:newNumber})){
      if(this.squareContainsEnemyPiece({letter:String.fromCharCode(letterCode),number:newNumber},color)){
        if(this.board[newNumber-1][letterCode-97].piece.constructor.name == 'King'){
          console.log('King check')
          return true;
        }
      }
    }

    newNumber = kingPosition.number - 1;
    if(newPiece.isPossibleMove({letter:String.fromCharCode(letterCode),number:newNumber})){
      if(this.squareContainsEnemyPiece({letter:String.fromCharCode(letterCode),number:newNumber},color)){
        if(this.board[newNumber-1][letterCode-97].piece.constructor.name == 'King'){
          console.log('King check')
          return true;
        }
      }
    }

    newNumber = kingPosition.number - 1;
    letterCode = kingPosition.letter.charCodeAt(0) - 1;
    if(newPiece.isPossibleMove({letter:String.fromCharCode(letterCode),number:newNumber})){
      if(this.squareContainsEnemyPiece({letter:String.fromCharCode(letterCode),number:newNumber},color)){
        if(this.board[newNumber-1][letterCode-97].piece.constructor.name == 'King'){
          console.log('King check')
          return true;
        }
      }
    }

    letterCode = kingPosition.letter.charCodeAt(0) - 1;
    if(newPiece.isPossibleMove({letter:String.fromCharCode(letterCode),number:newNumber})){
      if(this.squareContainsEnemyPiece({letter:String.fromCharCode(letterCode),number:newNumber},color)){
        if(this.board[newNumber-1][letterCode-97].piece.constructor.name == 'King'){
          console.log('King check')
          return true;
        }
      }
    }

  }

}
