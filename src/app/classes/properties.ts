import { Color } from "../constants/enums.enum";
import { Piece } from "../classes/piece";

export class Properties {
    color:Color = Color.White;
    letter:String = 'a';
    squareNumber:number = 2;
    piece:Piece = null;
}
