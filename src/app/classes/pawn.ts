
import { Piece } from "./piece";
import { Color } from './../constants/enums.enum';
import { Coordinates } from "./coordinates";

export class Pawn extends Piece {
    constructor(color: Color,initialPosition : Coordinates){
        super(color, initialPosition);
        this.findPossibleMoves();
        if(color == Color.White){
            this.image = '/assets/pionBlanc.png';
        }
        else{
            this.image = '/assets/pionNoir.png';
        }
    }
    findPossibleMoves() {
        this.possibleMoves = [];
        let newPosition = {letter:this.position.letter,number:this.position.number};
        let letterCode = this.position.letter.charCodeAt(0) - 1;
        let topLimit = letterCode + 2;
        if(this.color == Color.White){
            newPosition.number += 1;
        }
        else{
            newPosition.number -= 1;
        }
        while(letterCode <= topLimit){
            if(this.isPossibleMove({letter:String.fromCharCode(letterCode),number:newPosition.number})){
                this.possibleMoves.push({letter:String.fromCharCode(letterCode),number:newPosition.number});
            }
            letterCode++;
        }

        letterCode = this.position.letter.charCodeAt(0);
        if(this.position.number == 2 && this.color == Color.White){
            this.possibleMoves.push({letter:String.fromCharCode(letterCode),number:this.position.number+2})
        }

        else if(this.position.number == 7 && this.color == Color.Black){
            this.possibleMoves.push({letter:String.fromCharCode(letterCode),number:this.position.number-2})
        }
    }
}