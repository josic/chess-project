import { Piece } from "./piece";
import { Color } from './../constants/enums.enum';

export class Knight extends Piece {
    constructor(color: Color,initialPosition){
        super(color, initialPosition);
        this.findPossibleMoves();
        if(color == Color.White){
            this.image = '/assets/cavalierBlanc.png';
        }
        else{
            this.image = '/assets/cavalierNoir.png';
        }
    }
    findPossibleMoves() {
        this.possibleMoves = [];
        let newNumber = this.position.number + 1;
        let letterCode = this.position.letter.charCodeAt(0) - 2;
        if(this.isPossibleMove({letter:String.fromCharCode(letterCode),number:newNumber})){
            this.possibleMoves.push({letter:String.fromCharCode(letterCode),number:newNumber});
        }

        letterCode = this.position.letter.charCodeAt(0) + 2;
        if(this.isPossibleMove({letter:String.fromCharCode(letterCode),number:newNumber})){
            this.possibleMoves.push({letter:String.fromCharCode(letterCode),number:newNumber});
        }

        newNumber = this.position.number + 2;
        letterCode = this.position.letter.charCodeAt(0) - 1;
        if(this.isPossibleMove({letter:String.fromCharCode(letterCode),number:newNumber})){
            this.possibleMoves.push({letter:String.fromCharCode(letterCode),number:newNumber});
        }

        letterCode = this.position.letter.charCodeAt(0) + 1;
        if(this.isPossibleMove({letter:String.fromCharCode(letterCode),number:newNumber})){
            this.possibleMoves.push({letter:String.fromCharCode(letterCode),number:newNumber});
        }

        newNumber = this.position.number - 2;
        letterCode = this.position.letter.charCodeAt(0) - 1;
        if(this.isPossibleMove({letter:String.fromCharCode(letterCode),number:newNumber})){
            this.possibleMoves.push({letter:String.fromCharCode(letterCode),number:newNumber});
        }

        letterCode = this.position.letter.charCodeAt(0) + 1;
        if(this.isPossibleMove({letter:String.fromCharCode(letterCode),number:newNumber})){
            this.possibleMoves.push({letter:String.fromCharCode(letterCode),number:newNumber});
        }

        newNumber = this.position.number - 1;
        letterCode = this.position.letter.charCodeAt(0) - 2;
        if(this.isPossibleMove({letter:String.fromCharCode(letterCode),number:newNumber})){
            this.possibleMoves.push({letter:String.fromCharCode(letterCode),number:newNumber});
        }

        letterCode = this.position.letter.charCodeAt(0) + 2;
        if(this.isPossibleMove({letter:String.fromCharCode(letterCode),number:newNumber})){
            this.possibleMoves.push({letter:String.fromCharCode(letterCode),number:newNumber});
        }
    }
}
