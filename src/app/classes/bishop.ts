import { Piece } from "./piece";
import { Color } from './../constants/enums.enum';
import { cpuUsage } from "process";

export class Bishop extends Piece{
    constructor(color: Color,initialPosition){
        super(color, initialPosition);
        this.findPossibleMoves();
        if(color == Color.White){
            this.image = '/assets/fouBlanc.png';
        }
        else{
            this.image = '/assets/fouNoir.png';
        }
    }
    findPossibleMoves() {
        this.possibleMoves = [];
        let newPosition = {letter:this.position.letter,number:this.position.number + 1};
        let letterCode = this.position.letter.charCodeAt(0) + 1;
        while(this.isPossibleMove({letter:String.fromCharCode(letterCode),number:newPosition.number})){
            this.possibleMoves.push({letter:String.fromCharCode(letterCode),number:newPosition.number});
            letterCode++;
            newPosition.number++;
        }

        newPosition.number = this.position.number;
        newPosition.number -= 1;
        letterCode = this.position.letter.charCodeAt(0) - 1;
        while(this.isPossibleMove({letter:String.fromCharCode(letterCode),number:newPosition.number})){
            this.possibleMoves.push({letter:String.fromCharCode(letterCode),number:newPosition.number});
            letterCode--;
            newPosition.number--;
        }

        newPosition.number = this.position.number;
        newPosition.number -= 1;
        letterCode = this.position.letter.charCodeAt(0) + 1;
        while(this.isPossibleMove({letter:String.fromCharCode(letterCode),number:newPosition.number})){
            this.possibleMoves.push({letter:String.fromCharCode(letterCode),number:newPosition.number});
            letterCode++;
            newPosition.number--;
        }

        newPosition.number = this.position.number;
        newPosition.number += 1;
        letterCode = this.position.letter.charCodeAt(0) - 1;
        while(this.isPossibleMove({letter:String.fromCharCode(letterCode),number:newPosition.number})){
            this.possibleMoves.push({letter:String.fromCharCode(letterCode),number:newPosition.number});
            letterCode--;
            newPosition.number++;
        }
    }
}
