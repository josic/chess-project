
import { Piece } from "./piece";
import { Color } from './../constants/enums.enum';
import { Coordinates } from "./coordinates";

export class Rook extends Piece {
    constructor(color: Color,initialPosition: Coordinates){
        super(color, initialPosition);
        this.findPossibleMoves();
        if(color == Color.White){
            this.image = '/assets/tourBlanche.png';
        }
        else{
            this.image = '/assets/tourNoire.png';
        }
    }
    findPossibleMoves() {
        this.possibleMoves = [];
        let newPosition = {letter:this.position.letter,number:this.position.number};
        newPosition.number += 1;
        //vertical
        while(this.isPossibleMove(newPosition)){
            this.possibleMoves.push({letter:newPosition.letter,number:newPosition.number});
            newPosition.number++;
        }

        newPosition = {letter:this.position.letter,number:this.position.number};
        newPosition.number -= 1;
        while(this.isPossibleMove(newPosition)){
            this.possibleMoves.push({letter:newPosition.letter,number:newPosition.number});
            newPosition.number--;
        }
        //horizontal
        let letterCode = this.position.letter.charCodeAt(0) - 1;
        newPosition.number = this.position.number
        while(letterCode > 96){
            this.possibleMoves.push({letter:String.fromCharCode(letterCode),number:newPosition.number});
            letterCode --;
        }

        letterCode = this.position.letter.charCodeAt(0) + 1;
        while(String.fromCharCode(letterCode) != 'i'){
            this.possibleMoves.push({letter:String.fromCharCode(letterCode),number:newPosition.number});
            letterCode++;
        }
    }
}