import { Color } from './../constants/enums.enum';
import { Coordinates } from './coordinates';

export abstract class Piece {
    color: Color;
    position: Coordinates;
    possibleMoves: Coordinates[];
    validMoves: Coordinates[] = [];
    image: String;
    hasMoved:Boolean;

    constructor(color: Color, initialPosition:Coordinates){
        this.color = color;
        this.position = {letter:initialPosition.letter,number:initialPosition.number};
        this.hasMoved = false;
    }
    setPosition(newPosition){
        this.position = newPosition;
    }
    isPossibleMove(position: Coordinates){
        return this.isPossibleLetter(position.letter) && this.isPossibleNumber(position.number);
    }
    isPossibleLetter(letter:String){
        return 96 < letter.charCodeAt(0) && letter.charCodeAt(0) < 105
    }
    isPossibleNumber(number:number){
        return 0 < number && number < 9;
    }
    abstract findPossibleMoves();
}
