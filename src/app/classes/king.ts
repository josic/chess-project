import { Piece } from "./piece";
import { Color } from './../constants/enums.enum';
import { Coordinates } from "./coordinates";

export class King extends Piece {
    constructor(color: Color,initialPosition : Coordinates){
        super(color, initialPosition);
        this.findPossibleMoves();
        if(color == Color.White){
            this.image = '/assets/roiBlanc.png';
        }
        else{
            this.image = '/assets/roiNoir.png';
        }
    }
    findPossibleMoves() {
        this.possibleMoves = [];
        let newPosition = {letter:this.position.letter,number:this.position.number};
        newPosition.number += 1;
        //vertical
        if(this.isPossibleMove(newPosition)){
            this.possibleMoves.push({letter:newPosition.letter,number:newPosition.number});
        }

        newPosition = {letter:this.position.letter,number:this.position.number};
        newPosition.number -= 1;
        if(this.isPossibleMove(newPosition)){
            this.possibleMoves.push({letter:newPosition.letter,number:newPosition.number});
        }
        //horizontal
        let letterCode = this.position.letter.charCodeAt(0) - 1;
        newPosition.number = this.position.number
        if(letterCode > 96){
            this.possibleMoves.push({letter:String.fromCharCode(letterCode),number:newPosition.number});
        }

        letterCode = this.position.letter.charCodeAt(0) + 1;
        if(String.fromCharCode(letterCode) != 'i'){
            this.possibleMoves.push({letter:String.fromCharCode(letterCode),number:newPosition.number});
        }
        //diagonal
        newPosition.number += 1;
        letterCode = this.position.letter.charCodeAt(0) + 1;
        if(this.isPossibleMove({letter:String.fromCharCode(letterCode),number:newPosition.number})){
            this.possibleMoves.push({letter:String.fromCharCode(letterCode),number:newPosition.number});
        }

        newPosition.number = this.position.number;
        newPosition.number -= 1;
        letterCode = this.position.letter.charCodeAt(0) - 1;
        if(this.isPossibleMove({letter:String.fromCharCode(letterCode),number:newPosition.number})){
            this.possibleMoves.push({letter:String.fromCharCode(letterCode),number:newPosition.number});
        }

        newPosition.number = this.position.number;
        newPosition.number -= 1;
        letterCode = this.position.letter.charCodeAt(0) + 1;
        if(this.isPossibleMove({letter:String.fromCharCode(letterCode),number:newPosition.number})){
            this.possibleMoves.push({letter:String.fromCharCode(letterCode),number:newPosition.number});
        }

        newPosition.number = this.position.number;
        newPosition.number += 1;
        letterCode = this.position.letter.charCodeAt(0) - 1;
        if(this.isPossibleMove({letter:String.fromCharCode(letterCode),number:newPosition.number})){
            this.possibleMoves.push({letter:String.fromCharCode(letterCode),number:newPosition.number});
        }
    }
}
