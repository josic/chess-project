import { Component,AfterViewInit, ElementRef, HostListener, OnDestroy, ViewChild, OnInit, NgZone } from '@angular/core';
import { Coordinates } from './../classes/coordinates';
import { Queen } from './../classes/queen'
import { Color } from './../constants/enums.enum';
import { Knight } from './../classes/knight'
import { Rook } from './../classes/rook'
import { Bishop } from './../classes/bishop'
import { King } from './../classes/king'
import { Pawn } from './../classes/pawn'
import { MoveValidationService } from './../services/move-validation.service';
import { Properties } from '../classes/properties';
import { Piece } from '../classes/piece';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'echecs';

  row1:Properties[];
  row2:Properties[];
  row3:Properties[];
  row4:Properties[];
  row5:Properties[];
  row6:Properties[];
  row7:Properties[];
  row8:Properties[];
  board:Properties[][];
  isMoving:Boolean;
  isPromoting:Boolean;
  movingPosition:Coordinates;
  movingPiece:Piece;
  lastMovedColor:Color;
  
  
  constructor(public moveValidationService: MoveValidationService){
  }

  ngOnInit(): void {
    this.isMoving = false;
    this.lastMovedColor = Color.Black;
    this.createSquares();
    this.initializeBoard();
    this.moveValidationService.board = this.board;
  }

  createSquares(){
    let previousColor = Color.Black;
    let rowNumber = 1;
    this.row1 = [];
    for(let j = 97; j < 105;j++){
      previousColor = this.switchColors(previousColor);
      this.row1[j-97] = {color:previousColor,letter:String.fromCharCode(j),squareNumber:rowNumber,piece:null};
    }
    previousColor = this.switchColors(previousColor);
    rowNumber++;

    this.row2 = [];
    for(let j = 97; j < 105;j++){
      previousColor = this.switchColors(previousColor);
      this.row2[j-97] = {color:previousColor,letter:String.fromCharCode(j),squareNumber:rowNumber,piece:null};
    }
    previousColor = this.switchColors(previousColor);
    rowNumber++;

    this.row3 = [];
    for(let j = 97; j < 105;j++){
      previousColor = this.switchColors(previousColor);
      this.row3[j-97] = {color:previousColor,letter:String.fromCharCode(j),squareNumber:rowNumber,piece:null};
    }
    previousColor = this.switchColors(previousColor);
    rowNumber++;

    this.row4 = [];
    for(let j = 97; j < 105;j++){
      previousColor = this.switchColors(previousColor);
      this.row4[j-97] = {color:previousColor,letter:String.fromCharCode(j),squareNumber:rowNumber,piece:null};
    }
    previousColor = this.switchColors(previousColor);
    rowNumber++;

    this.row5 = [];
    for(let j = 97; j < 105;j++){
      previousColor = this.switchColors(previousColor);
      this.row5[j-97] = {color:previousColor,letter:String.fromCharCode(j),squareNumber:rowNumber,piece:null};
    }
    previousColor = this.switchColors(previousColor);
    rowNumber++;

    this.row6 = [];
    for(let j = 97; j < 105;j++){
      previousColor = this.switchColors(previousColor);
      this.row6[j-97] = {color:previousColor,letter:String.fromCharCode(j),squareNumber:rowNumber,piece:null};
    }
    previousColor = this.switchColors(previousColor);
    rowNumber++;

    this.row7 = [];
    for(let j = 97; j < 105;j++){
      previousColor = this.switchColors(previousColor);
      this.row7[j-97] = {color:previousColor,letter:String.fromCharCode(j),squareNumber:rowNumber,piece:null};
    }
    previousColor = this.switchColors(previousColor);
    rowNumber++;

    this.row8 = [];
    for(let j = 97; j < 105;j++){
      previousColor = this.switchColors(previousColor);
      this.row8[j-97] = {color:previousColor,letter:String.fromCharCode(j),squareNumber:rowNumber,piece:null};
    }
    previousColor = this.switchColors(previousColor);
    rowNumber++;

    this.board = [];
    this.board.push(this.row1);
    this.board.push(this.row2);
    this.board.push(this.row3);
    this.board.push(this.row4);
    this.board.push(this.row5);
    this.board.push(this.row6);
    this.board.push(this.row7);
    this.board.push(this.row8);
  }
  switchColors(previousColor:Color){
    if(previousColor == Color.White){
      return Color.Black;
    }
    else{
      return Color.White;
    }
  }

  initializeBoard(){
    let wQueen = new Queen(Color.White,{letter:'d',number:1});
    let bQueen = new Queen(Color.Black,{letter:'d',number:8});
    let wGKnight = new Knight(Color.White,{letter:'g',number:1});
    let wBKnight = new Knight(Color.White,{letter:'b',number:1});
    let bGKnight = new Knight(Color.Black,{letter:'g',number:8});
    let bBKnight = new Knight(Color.Black,{letter:'b',number:8});
    let wARook = new Rook(Color.White,{letter:'a',number:1});
    let wHRook = new Rook(Color.White,{letter:'h',number:1});
    let bARook = new Rook(Color.Black,{letter:'a',number:8});
    let bHRook = new Rook(Color.Black,{letter:'h',number:8});
    let wCBishop = new Bishop(Color.White,{letter:'c',number:1});
    let wFBishop = new Bishop(Color.White,{letter:'f',number:1});
    let bCBishop = new Bishop(Color.Black,{letter:'c',number:8});
    let bFBishop = new Bishop(Color.Black,{letter:'f',number:8});
    let wKing = new King(Color.White,{letter:'e',number:1});
    let bKing = new King(Color.Black,{letter:'e',number:8});
    let wAPawn = new Pawn(Color.White,{letter:'a',number:2});
    let wBPawn = new Pawn(Color.White,{letter:'b',number:2});
    let wCPawn = new Pawn(Color.White,{letter:'c',number:2});
    let wDPawn = new Pawn(Color.White,{letter:'d',number:2});
    let wEPawn = new Pawn(Color.White,{letter:'e',number:2});
    let wFPawn = new Pawn(Color.White,{letter:'f',number:2});
    let wGPawn = new Pawn(Color.White,{letter:'g',number:2});
    let wHPawn = new Pawn(Color.White,{letter:'h',number:2});
    let bAPawn = new Pawn(Color.Black,{letter:'a',number:7});
    let bBPawn = new Pawn(Color.Black,{letter:'b',number:7});
    let bCPawn = new Pawn(Color.Black,{letter:'c',number:7});
    let bDPawn = new Pawn(Color.Black,{letter:'d',number:7});
    let bEPawn = new Pawn(Color.Black,{letter:'e',number:7});
    let bFPawn = new Pawn(Color.Black,{letter:'f',number:7});
    let bGPawn = new Pawn(Color.Black,{letter:'g',number:7});
    let bHPawn = new Pawn(Color.Black,{letter:'h',number:7});

    this.row1[0].piece = wARook;
    this.row1[1].piece = wBKnight;
    this.row1[2].piece = wCBishop;
    this.row1[3].piece = wQueen;
    this.row1[4].piece = wKing;
    this.row1[5].piece = wFBishop;
    this.row1[6].piece = wGKnight;
    this.row1[7].piece = wHRook;

    this.row2[0].piece = wAPawn;
    this.row2[1].piece = wBPawn;
    this.row2[2].piece = wCPawn;
    this.row2[3].piece = wDPawn;
    this.row2[4].piece = wEPawn;
    this.row2[5].piece = wFPawn;
    this.row2[6].piece = wGPawn;
    this.row2[7].piece = wHPawn;

    this.row8[0].piece = bARook;
    this.row8[1].piece = bBKnight;
    this.row8[2].piece = bCBishop;
    this.row8[3].piece = bQueen;
    this.row8[4].piece = bKing;
    this.row8[5].piece = bFBishop;
    this.row8[6].piece = bGKnight;
    this.row8[7].piece = bHRook;

    this.row7[0].piece = bAPawn;
    this.row7[1].piece = bBPawn;
    this.row7[2].piece = bCPawn;
    this.row7[3].piece = bDPawn;
    this.row7[4].piece = bEPawn;
    this.row7[5].piece = bFPawn;
    this.row7[6].piece = bGPawn;
    this.row7[7].piece = bHPawn;
  }

  move(row:number, square:number){
    if(!this.isPromoting){
      if(this.isMoving){
        if(this.moveValidationService.isValidMove(this.movingPiece,row,square)){
          if(this.isCastling(row,square)){
            console.log('this.isCastling');
            this.castle(row,square);
          }
          else{
            this.board[this.movingPosition.number-1][this.movingPosition.letter.charCodeAt(0)-97].piece = null;
            this.movingPiece.position = {letter:this.board[row][square].letter,number:this.board[row][square].squareNumber};
            this.board[row][square].piece = this.movingPiece;
            this.movingPiece.hasMoved = true;
            this.checkIfPromoting();
          }
          this.lastMovedColor = this.movingPiece.color;
          this.lookForMates();
        }
      }
      else{
        if(this.board[row][square].piece != null && this.board[row][square].piece.color != this.lastMovedColor){
          this.movingPosition = {letter:this.board[row][square].piece.position.letter.valueOf(),number:this.board[row][square].piece.position.number.valueOf()};
          this.movingPiece = this.board[row][square].piece;
          this.isMoving = true;
          return;
        }
      }
      this.isMoving = false;
    }
  }
  
  isCastling(row:number,square:number):Boolean{
    console.log(this.movingPiece.hasMoved)
    if(this.movingPiece.constructor.name == "King" && !this.movingPiece.hasMoved){
      return this.movingPiece.position.letter.charCodeAt(0)-this.board[row][square].letter.charCodeAt(0) > 1 || this.movingPiece.position.letter.charCodeAt(0)-this.board[row][square].letter.charCodeAt(0) < -1;
    }
  }

  castle(row:number,square:number){
    if(square == 6){
      this.board[this.movingPosition.number-1][this.movingPosition.letter.charCodeAt(0)-97].piece = null;
      this.movingPiece.position = {letter:this.board[row][square].letter,number:this.board[row][square].squareNumber};
      this.movingPiece.hasMoved = true
      this.board[row][square].piece = this.movingPiece;
      
      let movingRook = this.board[this.movingPosition.number-1][7].piece;
      this.board[this.movingPosition.number-1][7].piece = null;
      movingRook.hasMoved = true
      movingRook.position = {letter:this.board[row][square-1].letter,number:this.board[row][square-1].squareNumber};
      this.board[row][square-1].piece = movingRook;
    }
    else{
      this.board[this.movingPosition.number-1][this.movingPosition.letter.charCodeAt(0)-97].piece = null;
      this.movingPiece.position = {letter:this.board[row][square].letter,number:this.board[row][square].squareNumber};
      this.movingPiece.hasMoved = true
      this.board[row][square].piece = this.movingPiece;
      
      let movingRook = this.board[this.movingPosition.number-1][0].piece;
      this.board[this.movingPosition.number-1][0].piece = null;
      movingRook.hasMoved = true
      movingRook.position = {letter:this.board[row][square+1].letter,number:this.board[row][square+1].squareNumber};
      this.board[row][square+1].piece = movingRook;
    }
  }
  lookForMates(){
    if(this.moveValidationService.isInCheck(this.switchColors(this.movingPiece.color))){
      if(this.moveValidationService.lookForCheckmate(this.switchColors(this.movingPiece.color))){
        window.alert('Checkmate');
      }
    }
    else{
      if(this.moveValidationService.lookForStalemate(this.switchColors(this.movingPiece.color))){
        window.alert('Stalemate');
      }
    }
  }

  checkIfPromoting(){
    if(this.movingPiece.constructor.name == 'Pawn'){
      if(this.movingPiece.color == Color.Black && this.movingPiece.position.number == 1){
        this.isPromoting = true;
      }
      else if(this.movingPiece.color == Color.White && this.movingPiece.position.number == 8){
        this.isPromoting = true;
      }
    }
  }

  promote(piece:String){
    if(this.movingPiece.color){
      for(let square of this.row8){
        this.promotePawn(square,piece);
      }
    }
    else{
      for(let square of this.row1){
        this.promotePawn(square,piece);
      }
    }
    this.isPromoting = false;
    console.log('promoted')
  }

  promotePawn(square:Properties,piece:String){
    if(square.piece.constructor.name == 'Pawn'){
      switch(piece){
        case 'Queen':{
          let newQueen = new Queen(this.movingPiece.color,{letter:square.letter,number:square.squareNumber})
          this.board[square.squareNumber-1][square.letter.charCodeAt(0)-97].piece = newQueen; 
          break;
        }
        case 'Rook':{
          let newRook = new Rook(this.movingPiece.color,{letter:square.letter,number:square.squareNumber});
          this.board[square.squareNumber-1][square.letter.charCodeAt(0)-97].piece = newRook;
          break;
        }
        case 'Bishop':{
          let newBishop = new Bishop(this.movingPiece.color,{letter:square.letter,number:square.squareNumber});
          this.board[square.squareNumber-1][square.letter.charCodeAt(0)-97].piece = newBishop;
          break;
        }
        case 'Knight':{
          let newKnight = new Knight(this.movingPiece.color,{letter:square.letter,number:square.squareNumber});
          this.board[square.squareNumber-1][square.letter.charCodeAt(0)-97].piece = newKnight;
          break;
        }
        default:{
          console.log('unknown piece');
          break;
        }
      }
    }
  }

}
