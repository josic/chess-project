
import { ApplicationRef, ElementRef, Component, ViewChild, Input, OnInit, HostListener } from '@angular/core';
import { Coordinates } from 'src/app/classes/coordinates';
import { Piece } from 'src/app/classes/piece';
import { Properties } from 'src/app/classes/properties';
import { Color } from 'src/app/constants/enums.enum';

@Component({
  selector: 'app-square',
  templateUrl: './square.component.html',
  styleUrls: ['./square.component.css']
})
export class SquareComponent implements OnInit{

  color:Color;
  piece:Piece;
  counter:number = 0;
  position:Coordinates;
  @Input() value:Properties;

  constructor() {
    this.piece = null;
  }
  ngOnInit(): void {
    this.modifySquare();
  }
  @HostListener('document:mousedown', ['$event'])
  modifySquare() {
    setTimeout(() => {
      this.color = this.value.color;
      this.position = {letter:this.value.letter,number:this.value.squareNumber};
      this.piece = this.value.piece;
      this.counter++;
    }, 0);
  }
}

